/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relations;

import oracle.kv.Key;
import oracle.kv.Value;

/**
 *
 * @author aimemath
 */
public abstract class Registered {
    protected int idSchool;
    protected int idStudent;

    public int getIdSchool() {
        return idSchool;
    }

    public void setIdSchool(int idSchool) {
        this.idSchool = idSchool;
    }

    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }
    
    public abstract Key getKey();
    
    public abstract Value getValue();
}
