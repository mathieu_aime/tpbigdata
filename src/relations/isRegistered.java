/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relations;

import java.util.Arrays;
import oracle.kv.Key;
import oracle.kv.Value;

/**
 *
 * @author aimemath
 */
public class isRegistered extends Registered {
    
    public static final String MAJOR_PATH = "isRegitered";

    public isRegistered(int idSchool, int idStudent) {
        this.idSchool = idSchool;
        this.idStudent = idStudent;
    }

    public isRegistered(byte[] bytes) {
        String s = new String(bytes);
        String[] ss = s.split("/");
        
        idStudent = Integer.parseInt(ss[0]);
        idSchool = Integer.parseInt(ss[1]);
    }

    @Override
    public String toString() {
        return idStudent + "/" + idSchool ;
    }
    
    @Override
    public Key getKey() {
        return Key.createKey( Arrays.asList(MAJOR_PATH, ""+idStudent));
    }
    
    @Override
    public Value getValue() {
        return Value.createValue(toString().getBytes());
    }    
}
