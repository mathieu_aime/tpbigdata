/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relations;

import java.util.Arrays;
import oracle.kv.Key;
import oracle.kv.Value;

/**
 *
 * @author aimemath
 */
public class hasRegistered extends Registered {
    
    public static final String MAJOR_PATH = "hasRegistered";
    private int rank;

    public hasRegistered(int idSchool, int rank, int idStudent) {
        this.idSchool = idSchool;
        this.idStudent = idStudent;
        this.rank = rank;
    }
    
    public hasRegistered(byte[] bytes) {
        String s = new String(bytes);
        String[] ss = s.split("/");
        
        idSchool = Integer.parseInt(ss[0]);
        rank = Integer.parseInt(ss[1]);
        idStudent = Integer.parseInt(ss[2]);
    }
    
    @Override
    public String toString() {
        return idSchool + "/" + rank + "/" + idStudent ;
    }
    
    @Override
    public Key getKey() {
        return Key.createKey( Arrays.asList(MAJOR_PATH, String.valueOf(idSchool), String.valueOf(rank)));
    }
    
    @Override
    public Value getValue() {
        return Value.createValue(toString().getBytes());
    }
}
