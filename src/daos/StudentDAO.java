/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import entities.Student;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import oracle.kv.Direction;
import oracle.kv.KVStore;
import oracle.kv.Key;
import oracle.kv.KeyValueVersion;
import oracle.kv.Value;
import oracle.kv.ValueVersion;

/**
 *
 * @author aimemath
 */
public class StudentDAO {

    private final KVStore store;

    public StudentDAO(KVStore s) {
        store = s;
    }

    public void create(int id, String name, String firstName, String city, String country) {
        Student student = new Student(id, name, firstName, city, country);
        store.putIfAbsent(student.getKey(), student.getValue());
    }

    public List<Student> getAll() {
        Key key = Key.createKey(Student.MAJOR_PATH);

        Iterator<KeyValueVersion> i = store.storeIterator(Direction.UNORDERED, 0, key, null, null);

        ArrayList<Student> students = new ArrayList<>();

        while (i.hasNext()) {
            Value v = i.next().getValue();
            students.add(new Student(v.getValue()));
        }

        return students;
    }

    public Student get(int id) {
        Student s = null;
        Key k = Key.createKey(Arrays.asList(Student.MAJOR_PATH, "" + id));
        ValueVersion valueVersionrecherche = store.get(k);

        if (valueVersionrecherche != null) {
            Value v = valueVersionrecherche.getValue();
            s = new Student(v.getValue());
        }
        return s;
    }

    public List<Student> getLimit(int n) {
        Key key = Key.createKey(Student.MAJOR_PATH);

        Iterator<KeyValueVersion> it = store.storeIterator(Direction.UNORDERED, 0, key, null, null);

        ArrayList<Student> students = new ArrayList<>();

        for (int i = 0; it.hasNext() && i < n; ++i) {
            Value v = it.next().getValue();
            students.add(new Student(v.getValue()));
        }

        return students;
    }

    public void delete(int id) {
        Key key = Key.createKey(Arrays.asList(Student.MAJOR_PATH, "" + id));
        store.delete(key);
    }

    public void deleteAll() {
        Key key = Key.createKey(Student.MAJOR_PATH);
        Iterator<KeyValueVersion> i = store.storeIterator(Direction.UNORDERED, 0, key, null, null);

        while (i.hasNext()) {
            store.delete(i.next().getKey());
        }
    }

    public void generate(int n) {
        for (int i = 0; i < n; ++i) {
            String name = "Name" + ThreadLocalRandom.current().nextInt(1, 1000);
            String firstName = "First Name" + ThreadLocalRandom.current().nextInt(1, 1000);
            int rc = ThreadLocalRandom.current().nextInt(1, 10);
            String country = "Country" + rc;
            String city = "City" + rc + ThreadLocalRandom.current().nextInt(1, 10);

            create(i, name, firstName, city, country);
        }
    }
}
