/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import oracle.kv.Direction;
import oracle.kv.KVStore;
import oracle.kv.Key;
import oracle.kv.KeyValueVersion;
import oracle.kv.Value;
import oracle.kv.ValueVersion;
import relations.hasRegistered;

/**
 *
 * @author aimemath
 */
public class HasRegisteredDAO {

    private KVStore store;

    public HasRegisteredDAO(KVStore s) {
        store = s;
    }

    public void create(int idSchool, int rank, int idStudent) {
        hasRegistered isReg = new hasRegistered(idSchool, rank, idStudent);
        store.putIfAbsent(isReg.getKey(), isReg.getValue());
    }

    public List<hasRegistered> getAll() {
        Key key = Key.createKey(hasRegistered.MAJOR_PATH);

        Iterator<KeyValueVersion> i = store.storeIterator(Direction.UNORDERED, 0, key, null, null);

        ArrayList<hasRegistered> regs = new ArrayList<>();

        while (i.hasNext()) {
            Value v = i.next().getValue();
            regs.add(new hasRegistered(v.getValue()));
        }

        return regs;
    }

    public List<hasRegistered> get(int id) {
        Key key = Key.createKey(Arrays.asList(hasRegistered.MAJOR_PATH, String.valueOf(id)));

        Iterator<KeyValueVersion> i = store.storeIterator(Direction.UNORDERED, 0, key, null, null);

        ArrayList<hasRegistered> regs = new ArrayList<>();

        while (i.hasNext()) {
            Value v = i.next().getValue();
            regs.add(new hasRegistered(v.getValue()));
        }

        return regs;
    }

    public hasRegistered get(int id, int rank) {
        hasRegistered hR = null;
        Key k = Key.createKey(Arrays.asList(hasRegistered.MAJOR_PATH, String.valueOf(id), String.valueOf(rank)));
        ValueVersion valueVersionrecherche = store.get(k);

        if (valueVersionrecherche != null) {
            Value v = valueVersionrecherche.getValue();
            hR = new hasRegistered(v.getValue());
        }
        return hR;
    }

    public void delete(int id, int rank) {
        Key key = Key.createKey(Arrays.asList(hasRegistered.MAJOR_PATH, String.valueOf(id), String.valueOf(rank)));
        store.delete(key);
    }

    public void delete(int id) {
        Key key = Key.createKey(Arrays.asList(hasRegistered.MAJOR_PATH, String.valueOf(id)));
        Iterator<KeyValueVersion> i = store.storeIterator(Direction.UNORDERED, 0, key, null, null);

        while (i.hasNext()) {
            Key k = i.next().getKey();
            store.delete(k);
        }
    }

    public void deleteAll() {
        Key key = Key.createKey(hasRegistered.MAJOR_PATH);
        Iterator<KeyValueVersion> i = store.storeIterator(Direction.UNORDERED, 0, key, null, null);

        while (i.hasNext()) {
            Key k = i.next().getKey();
            store.delete(k);
        }
    }
}
