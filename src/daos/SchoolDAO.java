/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import entities.School;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import oracle.kv.Direction;
import oracle.kv.KVStore;
import oracle.kv.Key;
import oracle.kv.KeyValueVersion;
import oracle.kv.Value;
import oracle.kv.ValueVersion;

/**
 *
 * @author aimemath
 */
public class SchoolDAO {

    private final KVStore store;

    public SchoolDAO(KVStore s) {
        store = s;
    }

    public void create(int id, String name, String city, String country) {
        School school = new School(id, name, city, country);
        store.putIfAbsent(school.getKey(), school.getValue());
    }

    public List<School> getAll() {
        Key key = Key.createKey(School.MAJOR_PATH);

        Iterator<KeyValueVersion> i = store.storeIterator(Direction.UNORDERED, 0, key, null, null);

        ArrayList<School> schools = new ArrayList<>();

        while (i.hasNext()) {
            Value v = i.next().getValue();
            schools.add(new School(v.getValue()));
        }

        return schools;
    }

    public School get(int id) {
        School s = null;
        Key k = Key.createKey(Arrays.asList(School.MAJOR_PATH, "" + id));
        ValueVersion valueVersionrecherche = store.get(k);
        if (valueVersionrecherche != null) {
            Value v = valueVersionrecherche.getValue();;
            s = new School(v.getValue());
        }
        return s;
    }

    public void delete(int id) {
        Key key = Key.createKey(Arrays.asList(School.MAJOR_PATH, "" + id));
        store.delete(key);
    }

    public void deleteAll() {
        Key key = Key.createKey(School.MAJOR_PATH);
        Iterator<KeyValueVersion> i = store.storeIterator(Direction.UNORDERED, 0, key, null, null);

        while (i.hasNext()) {
            Key k = i.next().getKey();
            store.delete(k);
        }
    }

    public void generate(int n) {
        for (int i = 0; i < n; ++i) {
            String name = "Name" + i;
            int rc = ThreadLocalRandom.current().nextInt(1, 10);
            String country = "Country" + rc;
            String city = "City" + rc + ThreadLocalRandom.current().nextInt(1, 3);

            create(i, name, city, country);
        }
    }
}
