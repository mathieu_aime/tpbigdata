/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import oracle.kv.Direction;
import oracle.kv.KVStore;
import oracle.kv.Key;
import oracle.kv.KeyValueVersion;
import oracle.kv.Value;
import oracle.kv.ValueVersion;
import relations.isRegistered;

/**
 *
 * @author aimemath
 */
public class IsRegisteredDAO {

    private final KVStore store;

    public IsRegisteredDAO(KVStore s) {
        store = s;
    }

    public void create(int idSchool, int idStudent) {
        isRegistered isReg = new isRegistered(idSchool, idStudent);
        store.putIfAbsent(isReg.getKey(), isReg.getValue());
    }

    public List<isRegistered> getAll() {
        Key key = Key.createKey(isRegistered.MAJOR_PATH);

        Iterator<KeyValueVersion> i = store.storeIterator(Direction.UNORDERED, 0, key, null, null);

        ArrayList<isRegistered> regs = new ArrayList<>();

        while (i.hasNext()) {
            Value v = i.next().getValue();
            regs.add(new isRegistered(v.getValue()));
        }

        return regs;
    }

    public isRegistered get(int id) {
        isRegistered isR = null;
        Key k = Key.createKey(Arrays.asList(isRegistered.MAJOR_PATH, "" + id));
        ValueVersion valueVersionrecherche = store.get(k);

        if (valueVersionrecherche != null) {
            Value v = valueVersionrecherche.getValue();
            isR = new isRegistered(v.getValue());
        }
        return isR;
    }

    public void delete(int id) {
        Key key = Key.createKey(Arrays.asList(isRegistered.MAJOR_PATH, "" + id));
        store.delete(key);
    }

    public void deleteAll() {
        Key key = Key.createKey(isRegistered.MAJOR_PATH);
        Iterator<KeyValueVersion> i = store.storeIterator(Direction.UNORDERED, 0, key, null, null);

        while (i.hasNext()) {
            Key k = i.next().getKey();
            store.delete(k);
        }
    }
}
