/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Arrays;
import oracle.kv.Key;
import oracle.kv.Value;

/**
 *
 * @author aimemath
 */
public class Student {
    private int id;
    private String name;
    private String firstName;
    private String city;
    private String country;
    
    public static final String MAJOR_PATH = "student";

    public Student(int id, String name, String firstName, String city, String country) {
        this.id = id;
        this.name = name;
        this.firstName = firstName;
        this.city = city;
        this.country = country;
    }
    
    public Student(byte[] bytes) {
        String s = new String(bytes);
        String[] ss = s.split("/");
        
        id = Integer.parseInt(ss[0]);
        name = ss[1];
        firstName = ss[2];
        city = ss[3];
        country = ss[4];
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return id + "/" + name + "/" + firstName + "/" + city + "/" + country;
    }
    
    public Key getKey() {
        return Key.createKey( Arrays.asList(MAJOR_PATH, ""+id));
    }
    
    public Value getValue() {
        return Value.createValue(toString().getBytes());
    }
}
