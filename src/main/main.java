package main;

import daos.HasRegisteredDAO;
import daos.IsRegisteredDAO;
import daos.SchoolDAO;
import daos.StudentDAO;
import entities.School;
import entities.Student;
import java.util.List;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import oracle.kv.Direction;
import oracle.kv.KVStore;
import oracle.kv.KVStoreConfig;
import oracle.kv.KVStoreFactory;
import oracle.kv.KeyValueVersion;
import relations.hasRegistered;
import relations.isRegistered;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author aimemath
 */
public class main {

    /**
     * @param args the command line arguments
     */
    private static final String storeName = "kvstore";
    private static final String hostName = "localhost";
    private static final String hostPort = "5000";

    private static final KVStoreConfig storeConfiguration = new KVStoreConfig(storeName, hostName + ":" + hostPort);
    private static final KVStore store = KVStoreFactory.getStore(storeConfiguration);

    private static StudentDAO studentDAO;
    private static SchoolDAO schoolDAO;
    private static IsRegisteredDAO isRegisteredDAO;
    private static HasRegisteredDAO hasRegisteredDAO;

    public static void main(String[] args) {

        studentDAO = new StudentDAO(store);
        schoolDAO = new SchoolDAO(store);
        isRegisteredDAO = new IsRegisteredDAO(store);
        hasRegisteredDAO = new HasRegisteredDAO(store);

        Map<Integer, Map<String, Long>> results = new HashMap<>();

        for (int i = 100; i < 500; i += 100) {
            clean();
            results.put(i, test(i));
        }

        toFile("results.csv", results);
    }

    private static Map<String, Long> test(int n) {
        Map<String, Long> times = new HashMap<>();

        int m = 1000 * n;
        int nbRequete = 1;
        //Generate 100000 students
        long startTime = System.currentTimeMillis();
        studentDAO.generate(m);
        long estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println(m + " students created in " + estimatedTime + " ms");
        times.put("Requete" + nbRequete++, estimatedTime);

        //Generate 100 schools
        startTime = System.currentTimeMillis();
        schoolDAO.generate(n);
        estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println(n + " schools created in " + estimatedTime + " ms");
        times.put("Requete" + nbRequete++, estimatedTime);

        //Genrerate relations
        startTime = System.currentTimeMillis();
        generateRelations(n);
        estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println(m + " relations student -> school & " + m + " school -> student created in " + estimatedTime + " ms");
        times.put("Requete" + nbRequete++, estimatedTime);

        //Get school for the 100st students
        startTime = System.currentTimeMillis();
        searchSchool();
        estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println("Search of the school of the " + n + "st students in " + estimatedTime + " ms");
        times.put("Requete" + nbRequete++, estimatedTime);

        //Get country for the school 1 students
        startTime = System.currentTimeMillis();
        searchCountry();
        estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println("Search of the country of the school 1 students in " + estimatedTime + " ms");
        times.put("Requete" + nbRequete++, estimatedTime);

        //Delete school 1
        startTime = System.currentTimeMillis();
        deleteSchool(1);
        estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println("Delete school 1 in " + estimatedTime + " ms");
        times.put("Requete" + nbRequete++, estimatedTime);

        searchCountry();

        //Delete 100000 relations isRegistered
        startTime = System.currentTimeMillis();
        isRegisteredDAO.deleteAll();
        estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println(m + " relations isRegistered deleted in " + estimatedTime + " ms");
        times.put("Requete" + nbRequete++, estimatedTime);

        //Delete 100000 relations hasRegistered
        startTime = System.currentTimeMillis();
        hasRegisteredDAO.deleteAll();
        estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println(m + " relations hasRegistered deleted in " + estimatedTime + " ms");
        times.put("Requete" + nbRequete++, estimatedTime);

        //Delete 100000 students
        startTime = System.currentTimeMillis();
        studentDAO.deleteAll();
        estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println(m + " students deleted in " + estimatedTime + " ms");
        times.put("Requete" + nbRequete++, estimatedTime);

        //Delete 100 schools
        startTime = System.currentTimeMillis();
        schoolDAO.deleteAll();
        estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println(n + " schools deleted in " + estimatedTime + " ms");
        times.put("Requete" + nbRequete++, estimatedTime);

        return times;
    }

    private static void toFile(String fileName, Map<Integer, Map<String, Long>> results) {
        try {
            PrintWriter writer = new PrintWriter(fileName, "UTF-8");

            for (Map.Entry<Integer, Map<String, Long>> m : results.entrySet()) {
                StringBuilder line = new StringBuilder();
                for (Map.Entry<String, Long> m2 : m.getValue().entrySet()) {
                    line.append(String.valueOf(m2.getKey()) + ";");
                }

                writer.println(line);
            }
            
            for (Map.Entry<Integer, Map<String, Long>> m : results.entrySet()) {
                StringBuilder line = new StringBuilder(String.valueOf(m.getKey()) + ';');
                for (Map.Entry<String, Long> m2 : m.getValue().entrySet()) {
                    line.append(String.valueOf(m2.getValue()) + ";");
                }

                writer.println(line);
            }

            writer.close();
        } catch (IOException e) {
            // do something
        }
    }

    private static void generateRelations(int n) {
        for (int student = 0; student < 1000 * n; ++student) {
            int school = ThreadLocalRandom.current().nextInt(1, n);
            isRegisteredDAO.create(school, student);
            hasRegisteredDAO.create(school, student, student);
        }
    }

    private static void searchSchool() {

        for (Student s : studentDAO.getLimit(100)) {
            isRegistered isR = isRegisteredDAO.get(s.getId());
            School school = schoolDAO.get(isR.getIdSchool());
            //System.out.println(s.getFirstName()+ " -> " + school.getName());
        }
    }

    private static void searchCountry() {
        for (hasRegistered hR : hasRegisteredDAO.get(1)) {
            Student student = studentDAO.get(hR.getIdStudent());
            //System.out.println(student.getCountry());            
        }
    }

    private static void deleteSchool(int id) {
        schoolDAO.delete(id);

        for (hasRegistered hR : hasRegisteredDAO.get(id)) {
            isRegisteredDAO.delete(hR.getIdStudent());
        }

        hasRegisteredDAO.delete(id);
    }

    public static void clean() {
        Iterator<KeyValueVersion> i = store.storeIterator(Direction.UNORDERED, 0);

        while (i.hasNext()) {
            store.delete(i.next().getKey());
        }
    }
}
